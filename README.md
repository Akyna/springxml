# Spring Xml #
### WEB ###
* Upload - The page for an upload of data (XML file) in the program.
* Show catalog - page to display the data (table of data to be displayed page by page)
* Download - The page with the link for a dounload of the integrated XML file

### Technology ###
* Spring MVC
* Tomcat 8.0.28
* JDK 1.8
* Maven 3.3.3
* HTML/CSS