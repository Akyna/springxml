package com.rightandabove.dao;

import com.rightandabove.model.Catalog;

import java.io.InputStream;

/**
 * Created by akyna on 11/4/15.
 */
public interface CatalogDao {

    public Catalog createCatalogFromXml(InputStream inputStream);
}
