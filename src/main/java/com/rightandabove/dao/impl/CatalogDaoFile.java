package com.rightandabove.dao.impl;

import com.rightandabove.dao.CatalogDao;
import com.rightandabove.model.Catalog;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.InputStream;

public class CatalogDaoFile implements CatalogDao {
    private static final String INNER_CATALOG = "/catalog.xml";

    public Catalog createCatalogFromXml(InputStream inputStream) {
        Catalog catalog = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Catalog.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            catalog = (Catalog) jaxbUnmarshaller.unmarshal(inputStream);
            inputStream.close();
            return catalog;
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return catalog;
    }
}
