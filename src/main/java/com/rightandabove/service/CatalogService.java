package com.rightandabove.service;

import com.rightandabove.dao.CatalogDao;
import com.rightandabove.model.Catalog;
import com.rightandabove.model.CompactDisk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.InputStream;
import java.util.Set;


public class CatalogService {
    @Autowired
    CatalogDao catalogDao;

    Logger logger = LoggerFactory.getLogger(CatalogService.class);
    private static final String INNER_CATALOG = "/catalog.xml";

    public Catalog createCatalogFromXml(InputStream inputStream) {
        return catalogDao.createCatalogFromXml(inputStream);
    }

    public void mergeCatalog(Catalog inputCatalog, Errors errors) {
        logger.info("Create object from inner xml");
        Catalog innerCatalog = catalogDao.createCatalogFromXml(CatalogService.class.getResourceAsStream(INNER_CATALOG));

        Set<CompactDisk> inputCompactDiskSet = inputCatalog.getCompactDisks();
        Set<CompactDisk> innerCompactDiskSet = innerCatalog.getCompactDisks();

        for (CompactDisk cd : inputCompactDiskSet) {
            if (innerCompactDiskSet.contains(cd)) {
                innerCompactDiskSet.remove(cd);
                innerCompactDiskSet.add(cd);
            } else {
                innerCompactDiskSet.add(cd);
            }
        }
        logger.info("Create result map from two map");
        try {
            File file = new File(CatalogService.class.getResource(INNER_CATALOG).getPath());
            JAXBContext jaxbContext = JAXBContext.newInstance(Catalog.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(innerCatalog, file);
        } catch (JAXBException e) {
            logger.info("Cannot create result merge two map. Exception: " + e.getMessage());
            errors.rejectValue("file", "", "Cannot find file in server!");
        }
    }

    public Set<CompactDisk> getAllCd() {
        Catalog catalog = catalogDao.createCatalogFromXml(CatalogService.class.getResourceAsStream(INNER_CATALOG));
        return catalog.getCompactDisks();
    }

}
