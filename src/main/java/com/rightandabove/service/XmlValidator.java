package com.rightandabove.service;

import com.rightandabove.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.xml.sax.SAXException;

import javax.servlet.ServletContext;
import javax.xml.XMLConstants;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.IOException;

public class XmlValidator implements Validator {
    private static final String SHEMA = "/catalog.xsd";

    @Autowired
    ServletContext context;

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    public void validate(Object object, Errors errors) {
        UploadedFile file = (UploadedFile) object;

        if (!file.getFile().getContentType().equalsIgnoreCase("text/xml") || file.getFile().getSize() == 0) {
            errors.rejectValue("file", "", "Please select xml file!");
        } else {
            try {
                SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                Schema schema = factory.newSchema(XmlValidator.class.getResource(SHEMA));
                javax.xml.validation.Validator validator = schema.newValidator();
                validator.validate(new StreamSource(file.getFile().getInputStream()));
            } catch (NullPointerException | IOException | SAXException e) {
                errors.rejectValue("file", "", "Please select a valid xml file!");
            }
        }
    }
}
