package com.rightandabove.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;

import java.util.*;


@XmlRootElement(name = "CATALOG")
public class Catalog {

    Set<CompactDisk> compactDisks = new LinkedHashSet<>();

    public Catalog() {
        this.compactDisks = new LinkedHashSet<>();
    }

    public Set<CompactDisk> getCompactDisks() {
        return compactDisks;
    }

    @XmlElement(name = "CD")
    public void setCompactDisks(Set<CompactDisk> compactDisks) {
        this.compactDisks = compactDisks;
    }
}

