package com.rightandabove.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Objects;

@XmlType(propOrder = {"title", "artist", "country", "company", "price", "year"})
public class CompactDisk {
    private String title;
    private String artist;
    private String country;
    private String company;
    private double price;
    private int year;

    public String getTitle() {
        return title;
    }

    @XmlElement(name = "TITLE")
    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    @XmlElement(name = "ARTIST")
    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getCountry() {
        return country;
    }

    @XmlElement(name = "COUNTRY")
    public void setCountry(String country) {
        this.country = country;
    }

    public String getCompany() {
        return company;
    }

    @XmlElement(name = "COMPANY")
    public void setCompany(String company) {
        this.company = company;
    }

    public double getPrice() {
        return price;
    }

    @XmlElement(name = "PRICE")
    public void setPrice(double price) {
        this.price = price;
    }

    public int getYear() {
        return year;
    }

    @XmlElement(name = "YEAR")
    public void setYear(int year) {
        this.year = year;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompactDisk that = (CompactDisk) o;
        return Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
}
