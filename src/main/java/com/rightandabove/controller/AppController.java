package com.rightandabove.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.rightandabove.model.Catalog;
import com.rightandabove.model.CompactDisk;
import com.rightandabove.service.CatalogService;
import com.rightandabove.service.XmlValidator;
import com.rightandabove.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.BufferedInputStream;
import java.io.FileInputStream;

import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.*;

import javax.servlet.http.HttpServletResponse;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AppController {

    @Autowired
    XmlValidator xmlValidator;

    @Autowired
    CatalogService catalogService;

    private static final String DOWNLOAD_FILE = "/catalog.xml";
    private static final String ERROR_MESSAGE = "Sorry. The file you are looking for doesn't exist";

    @RequestMapping("/")
    public ModelAndView welcomePage() {
        return new ModelAndView("welcome");
    }

    @RequestMapping("/uploadPage")
    public ModelAndView uploadFile() {
        return new ModelAndView("uploadFile");
    }

    @RequestMapping("/downloadPage")
    public ModelAndView getDownloadFile() {
        return new ModelAndView("downloadFile");
    }

    @RequestMapping("/catalog")
    public ModelAndView viewCatalog(@RequestParam(required = false) Integer page) {
        ModelAndView modelAndView = new ModelAndView("catalog");

        List<CompactDisk> list;

        try {
            list = new LinkedList<>(catalogService.getAllCd());
        } catch (IllegalArgumentException e) {
            return new ModelAndView("file-not-found");
        }
        PagedListHolder<CompactDisk> pagedListHolder = new PagedListHolder<>(list);
        pagedListHolder.setPageSize(5);
        modelAndView.addObject("maxPages", pagedListHolder.getPageCount());

        if (page == null || page < 1 || page > pagedListHolder.getPageCount()) page = 1;

        modelAndView.addObject("page", page);
        if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
            pagedListHolder.setPage(0);
            modelAndView.addObject("cdList", pagedListHolder.getPageList());
        } else if (page <= pagedListHolder.getPageCount()) {
            pagedListHolder.setPage(page - 1);
            modelAndView.addObject("cdList", pagedListHolder.getPageList());
        }

        return modelAndView;
    }

    @RequestMapping("/upload")
    public ModelAndView uploadFile(@ModelAttribute("uploadedFile") UploadedFile uploadedFile, BindingResult result) {
        Catalog catalog = new Catalog();

        try {
            xmlValidator.validate(uploadedFile, result);
        } catch (NullPointerException e) {
            return new ModelAndView("uploadFile");
        }
        if (result.hasErrors()) {
            return new ModelAndView("uploadFile");
        }
        try {
            catalog = catalogService.createCatalogFromXml(uploadedFile.getFile().getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            catalogService.mergeCatalog(catalog, result);
        } catch (IllegalArgumentException e) {
            return new ModelAndView("file-not-found");
        }

        return new ModelAndView("redirect:catalog");
    }

    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public void downloadFile(HttpServletResponse response) throws IOException {
        File file = null;
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();

        try {
            file = new File(classloader.getResource(DOWNLOAD_FILE).getFile());
        } catch (NullPointerException e) {
            OutputStream outputStream = response.getOutputStream();
            outputStream.write(ERROR_MESSAGE.getBytes(Charset.forName("UTF-8")));
            outputStream.close();
            return;
        }
        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
        if (mimeType == null) {
            mimeType = "application/octet-stream";
        }
        response.setContentType(mimeType);
        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
        response.setContentLength((int) file.length());
        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }

}