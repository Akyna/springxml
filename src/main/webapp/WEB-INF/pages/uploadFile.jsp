<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Upload Xml File</title>
</head>
<body>

<h3>Please select a file to upload !</h3>

<form:form method="post" enctype="multipart/form-data" modelAttribute="uploadedFile" action="upload">
    <table>
        <tr>
            <td>Upload File:</td>
            <td><input type="file" name="file"/>
            </td>
            <td style="color: red; font-style: italic;"><form:errors path="file"/>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Upload"/>
            </td>
            <td></td>
        </tr>
    </table>
</form:form>
<p>
    <a href="${pageContext.request.contextPath}/">Home </a>
</p>
</body>
</html>
