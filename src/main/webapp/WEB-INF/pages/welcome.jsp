<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome</title>
</head>
<body>
<div>
    <p>Welcome to home page</p>

    <p>
        <a href="${pageContext.request.contextPath}/uploadPage">Upload </a>
    </p>

    <p>
        <a href="${pageContext.request.contextPath}/downloadPage">Download</a>
    </p>

    <p>
        <a href="${pageContext.request.contextPath}/catalog">Show catalog</a>
    </p>
</div>
</body>
</html>
