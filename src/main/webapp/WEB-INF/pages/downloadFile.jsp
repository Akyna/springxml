<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Download Page</title>
</head>
<body>

<div>
    <a href="<c:url value='/download' />">Download</a>
</div>
<p>
    <a href="${pageContext.request.contextPath}/">Home </a>
</p>
</body>
</html>
