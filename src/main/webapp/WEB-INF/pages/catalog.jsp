<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List CD</title>
    <spring:url value="/resources/css/style.css" var="styleCss"/>
    <link href="${styleCss}" rel="stylesheet"/>
</head>
<body>

<table id="cdlist" style="width:100%;">
    <tr>
        <th>TITLE</th>
        <th>ARTIST</th>
        <th>COUNTRY</th>
        <th>COMPANY</th>
        <th>PRICE</th>
        <th>YEAR</th>
    </tr>
    <c:forEach var="cd" items="${cdList}">
        <tr>
            <td>${cd.title}</td>
            <td>${cd.artist}</td>
            <td>${cd.country}</td>
            <td>${cd.company}</td>
            <td>${cd.price}</td>
            <td>${cd.year}</td>
        </tr>
    </c:forEach>
</table>

<div id="pagination">
    <c:url value="/catalog" var="prev">
        <c:param name="page" value="${page-1}"/>
    </c:url>
    <c:if test="${page > 1}">
        <a href="<c:out value="${prev}" />" class="pn_prev">&larr;</a>
    </c:if>

    <c:forEach begin="1" end="${maxPages}" step="1" varStatus="i">
        <c:choose>
            <c:when test="${page == i.index}">
                <span>${i.index}</span>
            </c:when>
            <c:otherwise>
                <c:url value="/catalog" var="url">
                    <c:param name="page" value="${i.index}"/>
                </c:url>
                <a href='<c:out value="${url}" />'>${i.index}</a>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <c:url value="/catalog" var="next">
        <c:param name="page" value="${page + 1}"/>
    </c:url>
    <c:if test="${page + 1 <= maxPages}">
        <a href='<c:out value="${next}" />' class="pn_next"> &rarr; </a>
    </c:if>
</div>
<p>
    <a href="${pageContext.request.contextPath}/">Home </a>
</p>
</body>
</html>
